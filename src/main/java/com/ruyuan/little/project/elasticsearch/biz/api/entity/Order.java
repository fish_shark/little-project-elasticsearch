package com.ruyuan.little.project.elasticsearch.biz.api.entity;

import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@Data
public class Order {

    /**
     * 主键id
     */
    private String id;

    /**
     * 消费者手机号
     */
    private String phoneNumber;

    /**
     * 商品spuId
     */
    private String goodsSpuId;

    /**
     * 商品skuId
     */
    private String goodsSkuId;

    /**
     * 店铺Id
     */
    private String goodsStoreId;

    /**
     * 商品编号
     */
    private String goodsSpuNo;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 商品图片
     */
    private String goodsPicture;

    /**
     * 商品颜色
     */
    private String goodsColor;

    /**
     * 商品存储容量
     */
    private String goodsMemoryCapacity;

    /**
     * 商品价格
     */
    private Double goodsPrice;

    /**
     * 购买数量
     */
    private Integer payCount;

    /**
     * 订单状态 {@link com.ruyuan.little.project.elasticsearch.biz.api.enums.OrderStatusEnum}
     */
    private String status;

    /**
     * 订单状态描述
     */
    private String statusName;


    /**
     * 订单创建时间
     */
    private String createTime;

    /**
     * 订单支付时间
     */
    private String payTime;

    /**
     * 订单取消时间
     */
    private String cancelTime;

    /**
     * 完成时间
     */
    private String finishTime;

    /**
     * 收货时间
     */
    private String receiveTime;

    /**
     * 总价
     */
    private Double totalPrices;

    /**
     * 支付金额
     */
    private Double payAmount;

    /**
     * 评分
     */
    private Integer commentScore;

    /**
     * 评价
     */
    private String commentContent;
}
