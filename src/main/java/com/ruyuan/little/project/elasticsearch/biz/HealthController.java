package com.ruyuan.little.project.elasticsearch.biz;

import com.ruyuan.little.project.common.dto.CommonResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战，健康检查接口
 **/
@RestController
public class HealthController {

    @RequestMapping("/")
    public CommonResponse health(){
        return CommonResponse.success();
    }
}
